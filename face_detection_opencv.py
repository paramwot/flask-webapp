import cv2
import os
BASE_DIR = os.path.dirname(os.path.abspath(__file__))

class DetectFace:
    def __init__(self):
        super().__init__()

        # Get user supplied values
        self.cascPath = "haarcascade_frontalface_default.xml"

        # Create the haar cascade
        self.faceCascade = cv2.CascadeClassifier(self.cascPath)

    def detect_face(self, file_path):
        print("--------------------face detection---------------------")
        # file_path = f'static/uploads/{image_path}'
        print(f'img_path: {file_path}')
        # Read the image
        image = cv2.imread(file_path)
        print(f'read: {file_path}')
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        print("converted to Grayscale")

        faces = self.faceCascade.detectMultiScale(
            gray,
            scaleFactor=1.1,
            minNeighbors=5,
            minSize=(30, 30),
            flags=cv2.CASCADE_SCALE_IMAGE
        )

        print("Found {0} faces!".format(len(faces)))
        print(f'faces: {faces}')
        # Draw a rectangle around the faces
        for (x, y, w, h) in faces:
            cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 2)
            crop_img = image[y:y + h, x:x + w]
            file_name = file_path.split("/")[-1].split(".")[0]
            print(f'file_name variable: {file_name}')
            cv2.imwrite(os.path.join(BASE_DIR, f'static/display/{file_name}_cropped.jpg'), crop_img)
            print("imwrite success!")
            # cv2.waitKey(0)
        print("------------------------face detection ends----------------------------")
        return len(faces)
# instance = DetectFace()
# instance.detect_face("test1.jpeg")
