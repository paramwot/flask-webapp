import os
# import face_detection_opencv
import face_detection_mtcnn
from app import app
from flask import Flask, flash, request, render_template, jsonify, redirect, url_for, session, g
from werkzeug.utils import secure_filename

ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/login')
def upload_form():
    if 'username' in session:
        return render_template('index.html', user=session['user'])
    return redirect(url_for('login'))

@app.route('/', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        session['username'] = request.form['username']
        return redirect(url_for('upload_form'))
    return render_template('login.html')

@app.before_request
def before_request():
    if 'username' not in session and request.endpoint != "login":
        return redirect(url_for('login'))


@app.route('/logout')
def logout():
    session.pop('username', None)
    return redirect(url_for('login'))

@app.route('/image_upload', methods=['POST'])
def upload_image():
    if request.method == "POST":
        print("Inside Post")
        if 'file' not in request.files:
            flash('No file part')
            return jsonify({"data": "Input something"})
        file = request.files['file']
        if file.filename == '':
            flash('No image selected for uploading')
            return jsonify({"data": "Input something"})
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            print("image posted")
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            face_detection_mtcnn.detect_face(f'static/uploads/{filename}')
            print(f"filename: {filename}")
            if face_detection_mtcnn.detect_face(f'static/uploads/{filename}')['number_of_face'] == 0:
                flash("No Face found!")
                return jsonify({"data": "success", "name": 'Stop.png', "faces": 0})
            flash('Image successfully uploaded and displayed below')
            name = filename.split(".")[0]
            return jsonify({"data": "success", "name": name, "user_data": face_detection_mtcnn.detect_face(f'static/uploads/{filename}')})
        else:
            flash('Allowed image types are -> png, jpg, jpeg')
            return jsonify({"data": "fail"})


if __name__ == "__main__":
    app.run(debug=True)
