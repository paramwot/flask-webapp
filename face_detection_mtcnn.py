# Required libraries
from matplotlib import pyplot
from mtcnn.mtcnn import MTCNN
import cv2
import json
import os
BASE_DIR = os.path.dirname(os.path.abspath(__file__))


def detect_face(filename):
    # load image from file
    pixels = pyplot.imread(filename)
    # create the detector, using default weights
    detector = MTCNN()
    # detect faces in the image
    faces = detector.detect_faces(pixels)
    for face in faces:
        # print(face)
        data_points = {
            "number_of_face": len(faces),
            "face_box": face['box'],
            "confidance": face['confidence'],
            "left_eye_keypoints": face['keypoints']['left_eye'],
            "right_eye_keypoints": face['keypoints']['right_eye'],
            "nose_keypoints": face['keypoints']['nose'],
            "mouth_left_keypoints": face['keypoints']['mouth_left'],
            "mouth_right_keypoints": face['keypoints']['mouth_right']
        }
    print(f"number of faces: {len(faces)}")
    img = cv2.imread(filename)
    crop_img = img[face['box'][1]:face['box'][1] + face['box'][3], face['box'][0]:face['box'][0] + face['box'][2]]
    name = filename.split(".")[0]
    cv2.imwrite(os.path.join(BASE_DIR, f'static/display/{name}_cropped.jpg'), crop_img)
    return data_points
# print(detect_face("test1.jpeg")['number_of_face'])