# README #

### What is this repository for? ###

* This repository contains flask server for face detection API

### static directory contains files:
* `upload` directory for uploads
* `display` directory for results of face detection
* `app.js & particle.js` for particle js module 
* `style.css` for CSS

### templates directory contains files for html pages:
* currently `login.html` & `index.html` being used

****
* `main.py` is the main python file for flask server
* `face_detection_mtcnn.py` file for face detection

### How do I run? ###

* `python main.py` to run server
* enter same username and password to login
* upload image after login (May take while to give result; before showing results it will show 'success' pop-up message)
* type -> `localhost:5000/logout` to logout from session
